# -*- coding: utf-8 -*-
"""
Created on Sat Apr 13 19:46:45 2019

@author: Josh
"""
#----------------------------------------------------------------------------------------#
from main import np
from main import plt
#----------------------------------------------------------------------------------------#

def Partial_V_X(dx, grid_y, h, v_y):
    term1 = np.roll(h, -1, axis = 1) - h
#    print(np.roll(h, -1, axis = 1), h, 'term1')
#    print(np.roll(h, -1, axis = 1) -  h, 'term1')
    term2 = grid_y  * v_y
#    print(grid_y  * v_y, 'term2')
    return - term1 / dx + term2 

def Partial_V_Y(dy, grid_y, h, v_x):
    return - (np.roll(h, -1, axis = 0) - h) / dy - grid_y  * v_x

def Partial_H_Predictor(dx, dy, v_x, v_y):
#    print(np.roll(v_x, -1, axis = 1)[-1,-1])
#    print(v_x[-1,-1])
#    print(np.roll(v_y, -1, axis = 1)[-1,-1])
#    print(v_y[-1,-1])
    term1 = ( np.roll(v_x, -1, axis = 1) - v_x ) / dx
    term2 = ( np.roll(v_y, -1, axis = 0) - v_y ) / dy
#    print(np.roll(v_x, -1, axis = 1) - v_x, 'here')
#    print(np.roll(v_x, -1, axis = 1), 'here')
#    print(v_x, 'here')
#    print(np.roll(v_y, 1, axis = 0) - v_y, 'here')
#    print(np.roll(v_y, 1, axis = 0), 'here')
#    print(v_y, 'here')
#    print(term1 , term2)
    return - term1 - term2

#----------------------------------------------------------------------------------------#
    
def Partial_vx_bar(dx, grid_y, h_bar, vy_bar):
    return - (h_bar - np.roll(h_bar, 1, axis = 1)) / dx + grid_y  * vy_bar

def Partial_vy_bar(dy, grid_y, h_bar, vx_bar):
    return - (h_bar - np.roll(h_bar, 1, axis = 0)) / dy - grid_y  * vx_bar

def Partial_h_bar(dx, dy, vx_bar, vy_bar):
    term1 = (vx_bar - np.roll(vx_bar, 1, axis = 1) ) / dx
#    print(vx_bar, 'hi')
#    print(np.roll(vx_bar, 1, axis = 1))
#    plt.imshow(term1)
   
    term2 = (vy_bar - np.roll(vy_bar, -1, axis = 0) ) / dy
#    print(vy_bar, 'term2')
#    print(np.roll(vy_bar, -1, axis = 0), 'term2')
#    plt.imshow(-term1 -term2)
    return - term1 - term2

#----------------------------------------------------------------------------------------#
    









