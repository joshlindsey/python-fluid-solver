# -*- coding: utf-8 -*-
"""
Created on Sat Apr 13 19:48:22 2019

@author: Josh
"""
#----------------------------------------------------------------------------------------#
from main import np

#----------------------------------------------------------------------------------------#

def Barred(u, pd_u, dt):
    return u + pd_u * dt 

def Step(u, pd_u, pd_u_bar, dt):
    return u + (1/2)*(pd_u + pd_u_bar)*dt

#----------------------------------------------------------------------------------------#
    
def velocity(vx, vy):
    return np.sqrt(vx**2 + vy**2)

def time(c, dx, dy, dt, v):
    maximus = np.amax(v)
    term1 = [dx, dy]
    delta = min(term1)
    dt_new = c * delta / maximus
    if (dt_new > 1.25 * dt):
        return 1.25 * dt
    else:
        return dt_new

#----------------------------------------------------------------------------------------#