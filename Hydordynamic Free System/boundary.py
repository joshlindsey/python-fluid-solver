# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 08:32:43 2019

@author: Josh
"""

import numpy as np

def Periodic_Bound(u, ghost):
#    print(u,'u',len(u))
    
#    print(ghost,'ghost',len(ghost))
#    len_g = len(ghost)    
    
#    top = np.zeros(len_g)
#    bottom = np.zeros(len_g)
#    left = np.zeros(len_g)
#    right = np.zeros(len_g)
    
#    print(ghost,'ghost')
    #u = np.reshape(u, (3,3))
#    print(u,'u')
    
#    print(u[:,0], 'u left')
#    print(u[:,-1], 'u right')
#    print(u[0,:], 'u top')
#    print(u[-1,:], 'u bottom')
    
#    print('here mother fucker')

#    print(ghost[:,0],'im')
#    print(u[:,-1],'-1')
#    print(u[:,-1],'-2')
    
#    ghost[:,0]   = u[:,-2]         #left
#    ghost[:,-1]  = u[:,1] 
#    ghost[0,:]   = u[-2,:]         #top
#    ghost[-1,:]  = u[1,:]           #bottom
    
    u[:,0]   = u[:,-2]         #left
    u[:,-1]  = u[:,1] 
    u[0,:]   = u[-2,:]         #top
    u[-1,:]  = u[1,:]           #bottom
    
#    ghost[:,0]   = 2 * u[:,-1] - u[:,-2]        #left
#    ghost[:,-1]  = 2 * u[:,0] - u[:,1]          #right
#    ghost[0,:]   = 2 * u[-1,:] - u[-2,:]        #top
#    ghost[-1,:]  = 2 * u[0,:] - u[1,:]          #bottom
    
#    print(ghost[:,0],'here')
    
    NW = np.array([ghost[0,1], ghost[1,0]])
    NE = np.array([ghost[0,-2], ghost[1,-1]])
    SW = np.array([ghost[-1,1], ghost[-2,0]])
    SE = np.array([ghost[-1,-2], ghost[-2,-1]])
    
    NW = np.average(NW)
    NE = np.average(NE)
    SW = np.average(SW)
    SE = np.average(SE)
    
    #print(NW, NE, SW, SE)
    
    ghost[0,0] = NW
    ghost[0,-1] = NE
    ghost[-1,0] = SW
    ghost[-1,-1] = SE
    
    def pad_with(vector, pad_width, iaxis, kwargs):
         pad_value = kwargs.get('padder', 0)
         vector[:pad_width[0]] = pad_value
         vector[-pad_width[1]:] = pad_value
         return vector
     
    #u = np.pad(u, 1, pad_with)
    
#    u =  ghost + u
#    print(u,'u')
    
#    print(ghost,'ghost')
    
    return u, ghost







