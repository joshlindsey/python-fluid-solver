# -*- coding: utf-8 -*-
"""
Created on Sat Mar 30 13:56:27 2019♠

@author: Josh

'''
np.roll: axis= 1 for dx,  and axis = 0 for dy
'''

"""
#----------------------------------------------------------------------------------------#
import matplotlib.pyplot as plt
import numpy as np
import boundary as bd
import partial_derivatives as partial
import yeti
from matplotlib import animation

#----------------------------------------------------------------------------------------#
if __name__ == "__main__":
    #------------------------------------------------------------------------------------#
    #Simulation Paramters, Number of grid points within the computaional domain  
    grid_size = 5
    C =  0.5
    T = 0
    T_Final = 2000
    num = 11
    
    #------------------------------------------------------------------------------------#
    #Initialize grid variables
    x_nodes = num                             #Make sure the second decimal is always one
    y_nodes = num                             #Make sure the second decimal is always one
    half_x_domain = (x_nodes - 1) / 2
    half_y_range = (y_nodes - 1) / 2
 
    #------------------------------------------------------------------------------------#
    #Initialize x and y grid 
    x = np.linspace(- grid_size, grid_size, x_nodes)
    y = np.linspace(- grid_size, grid_size, y_nodes)
  
    #------------------------------------------------------------------------------------#
    #Dt will need to be changed later to be C times the minimum at       
    Dx = abs(x[0] - x[1])
    Dy = abs(y[0] - y[1])
    
    #------------------------------------------------------------------------------------#
    #Create arrays to work and store data
    #Grid
    X_Grid = np.zeros([x_nodes, x_nodes])
    Y_Grid = np.zeros([y_nodes , y_nodes])

    #------------------------------------------------------------------------------------#
    #initial primitive variables
    Vx_i   = np.zeros([x_nodes, x_nodes])
    Vy_i   = np.zeros([y_nodes, y_nodes])
    H_i    = np.zeros([x_nodes, y_nodes])
    
    #------------------------------------------------------------------------------------#
    #initial ghost zones
    GZ_Vx  = np.zeros([x_nodes, x_nodes])
    GZ_Vy  = np.zeros([y_nodes, y_nodes])
    GZ_H   = np.zeros([x_nodes, y_nodes]) 
    
    #------------------------------------------------------------------------------------#
    #Data bank
    Vx_List = [] 
    Vy_List = [] 
    H_List  = [] 
    V_List  = []

    #------------------------------------------------------------------------------------#
    #Initialize variables
    initial_VX = 10
    initial_VY = 10
    initial_H  = 10

    #------------------------------------------------------------------------------------#
    #Set up initial Conditions
    for i in range(len(x)):
        for j in range(len(y)):
            #----------------------------------------------------------------------------#
            #Reformat grid for beta plane
            X_Grid[i,j] = j - half_x_domain
            Y_Grid[i,j] = half_y_range - i
            #----------------------------------------------------------------------------#
            #Initial Conditons for vx and vy
            if (i == half_x_domain and j == half_y_range):
                Vx_i[i,j] = initial_VX
                Vy_i[i,j] = initial_VY
            else:
                Vx_i[i,j] = initial_VX
                Vy_i[i,j] = initial_VY
            #----------------------------------------------------------------------------#
            #Initial Conditons for H
            if (i == half_y_range + 1):
                H_i[i,j] = 10
            else:
                H_i[i,j] = initial_H     
                
    #------------------------------------------------------------------------------------#
    #Append initial conditions to data bank
    Vx_List.append(Vx_i)
    Vy_List.append(Vy_i)
    H_List.append(H_i)
    V_List.append(yeti.velocity(Vx_i, Vy_i))
    Velocity = yeti.velocity(Vx_i, Vy_i)
    Dt = yeti.time(C, Dx, Dy, 1, Velocity)
    
    #------------------------------------------------------------------------------------#
    #------------------------------------------------------------------------------------#
    while T < T_Final:
        #--------------------------------------------------------------------------------#
        Partial_VX = partial.Partial_V_X(Dx, Y_Grid, H_i, Vy_i)
        Partial_VY = partial.Partial_V_Y(Dy, Y_Grid, H_i, Vx_i)
        Partial_H = partial.Partial_H_Predictor(Dx, Dy, Vx_i, Vy_i)

        #--------------------------------------------------------------------------------#    
        VX_Bar = yeti.Barred(Vx_i, Partial_VX, Dt)
        VY_Bar = yeti.Barred(Vy_i, Partial_VY, Dt)
        H_Bar = yeti.Barred(H_i, Partial_H, Dt)

        #--------------------------------------------------------------------------------#
        Partial_VX_Bar = partial.Partial_vx_bar(Dx, Y_Grid, H_Bar, VY_Bar)
        Partial_VY_Bar = partial.Partial_vy_bar(Dy, Y_Grid, H_Bar, VX_Bar)
        Partial_H_Bar = partial.Partial_h_bar(Dx, Dy, VX_Bar, VY_Bar)

        #--------------------------------------------------------------------------------#        
        VX_Step = yeti.Step(Vx_i, Partial_VX, Partial_VX_Bar, Dt)
        VY_Step = yeti.Step(Vy_i, Partial_VY, Partial_VY_Bar, Dt)
        H_Step = yeti.Step(H_i, Partial_H, Partial_H_Bar, Dt)
        
        #--------------------------------------------------------------------------------#
        VX_New = bd.Periodic_Bound(VX_Step, GZ_Vx)[0]
        VY_New = bd.Periodic_Bound(VY_Step, GZ_Vy)[0]
        H_New  = bd.Periodic_Bound(H_Step, GZ_H)[0]
        V_New  = yeti.velocity(VX_New, VY_New)
        
        #--------------------------------------------------------------------------------#
        #Save new values
        Vx_List.append(VX_New)
        Vy_List.append(VY_New)
        H_List.append(H_New)
        V_List.append(V_New)
        
        #--------------------------------------------------------------------------------#
        #Update inital values as new for next iteration
        Vx_i = VX_New
        Vy_i = VY_New
        H_i = H_New
        
        #--------------------------------------------------------------------------------#
        Velocity = yeti.velocity(Vx_i, Vy_i)    
        plt.imshow(Velocity)
        Dt = yeti.time(C, Dx, Dy, Dt, Velocity)
        
        #--------------------------------------------------------------------------------#
        #print('Itteration T = {}'.format(T))
        T += 1
        if (T % 500 ==0):
            print(T)
        #--------------------------------------------------------------------------------#
        #Block to plot each iteraiton
        '''
        plt.clf()
        plt.title('Pythonspot.com heatmap example')
        plt.ylabel('y')
        plt.xlabel('x')
        heatmap = plt.imshow(H_i, interpolation= 'lanczos', cmap='viridis')
        plt.colorbar(heatmap)
        plt.show()
        '''
        #--------------------------------------------------------------------------------#
        #--------------------------------------------------------------------------------#
        #End of while loop
    
    #------------------------------------------------------------------------------------#
    #Function to set up quiver and save mp4
    '''
    X, Y = X_Grid, Y_Grid
    U, V =  Vx_List[0], Vy_List[0]
    fig, ax = plt.subplots(1,1)
    Q = ax.quiver(X, Y, U, V, pivot='mid', color='b', units='inches')    
    ax.set_xlim(- half_x_domain - 1, half_x_domain + 1)
    ax.set_ylim(- half_y_range - 1, half_y_range + 1)
    
    def update_quiver(nn, Q, X, Y):
        U = Vx_List[nn]
        V = Vy_List[nn]
        Q.set_UVC(U,V)
        return Q,
    
    anim = animation.FuncAnimation(fig, update_quiver, fargs=(Q, X, Y), interval=100, frames = T_Final, blit=False)
    fig.tight_layout()
    #plt.show()
    anim.save('vector_filed-1.mp4')
    '''
    #------------------------------------------------------------------------------------#
    #Function to save height heat map

    fig, ax = plt.subplots(1,1)
    ims = []
    
    for i in range(T_Final):
        heatmap = plt.imshow(H_List[i], animated=True, interpolation= 'spline36', cmap='viridis')
        #im = plt.imshow(H_List[i], animated=True)
        ims.append([heatmap])
    
    ani = animation.ArtistAnimation(fig, ims, interval=50, blit=True)
    fig.tight_layout()
    #plt.show()
    ani.save('Animations/heat_map-7.mp4')

    #--------------------------------------------------------------------------------#
    #End of Code

        
    
    
    

    
    
    
    
    
    
    
