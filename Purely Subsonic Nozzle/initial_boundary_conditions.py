# -*- coding: utf-8 -*-
"""
'''
'initial_boundary_conditions' module
    -This module is used to calculate the geometry of the nozzle and implement the
boundary conditions.
'''
"""

#-----------------------------------------------------------------------------#
from main import np

#nozzle area
def Area(x):
    if (x <= 1.5):
        return 1 + 2.2*(x - 1.5)**2
    else:
        return 1 + 0.2223*(x - 1.5)**2
    
#-----------------------------------------------------------------------------#
#initial conditions
def Rho(x):
    return 1 - 0.023 * x

def Temp(x):
    return 1 - 0.009333 * x

def Velocity(x):
    return 0.05 + 0.11 * x

#-----------------------------------------------------------------------------#
#boundary conditions
def outflow_boundary(vrt):
    vrt[-1] = 2 * vrt[-2] - vrt[-3]
    
def outflow_boundary_coupled(spec_val, rho_0, T_0):
    T_0[-1] = spec_val / np.asarray(rho_0[-1])

def inflow_boundary_flt(v):
    v[0] = 2 * v[1] - v[2]
    
def inflow_boundary(rt):
    rt[0] = 1
#-----------------------------------------------------------------------------#

