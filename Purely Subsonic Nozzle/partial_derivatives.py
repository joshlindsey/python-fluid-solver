# -*- coding: utf-8 -*-
"""
'''
'partial_derivative' module;

   -This module is used to calculate the partial derivatives for botht he predictor
and corrector steps. They may seem vary familiar. This is because they are derived 
from the same function; but, the predictor step uses a forward difference and the 
corrector step uses a backward difference.

   -Notice that here these functions are being passed lists and arrays. The functions
use the np.roll() functino to calculate each operation simulatiounsly. There 
maybe some confusion with the np.asarry() function. This is used where the
object being passed is a list instead of an array. Lists can not be operated 
on like arrays; although, if one list is an array, then that array can operate
on a list; hence the np.asarray() function. The np.roll() functions automatically
converst a list to array.
'''

"""

#-----------------------------------------------------------------------------#
from main import np
    
#-----------------------------------------------------------------------------#
#Partial derivatives used for the predictor step, forward difference
def partial_rho(A, rho_0, V_0, dx):
    term1 = rho_0 * ( np.roll(V_0, -1) - V_0 ) / (-dx)
    term2 = rho_0 * (V_0 * ( np.log(np.roll(A, -1)) - np.log(A)) ) / (-dx)
    term3 = V_0 * (np.roll(rho_0, -1) - rho_0) / (-dx)
    return term1 + term2 + term3

def partial_velocity(rho_0, T_0, V_0, dx, Gamma):
    term1 = V_0 * ( np.roll(V_0, -1) - V_0 ) / (-dx)
    term2 = (np.roll(T_0, - 1) - T_0) / (dx)
    term3 = np.asarray(T_0) * (np.roll(rho_0, -1) - rho_0) / ((dx) * np.asarray(rho_0))
    return term1 - (1/Gamma) * (term2 + term3)

def partial_temp(A, T_0, V_0, dx, Gamma):
    term1 = V_0 * (np.roll(T_0, - 1) - T_0) / (-dx)
    term2 = np.asarray(T_0) * (Gamma - 1)
    term3 = (np.roll(V_0, - 1) - V_0) / (dx) 
    term4 = V_0 * ( np.log(np.roll(A, -1)) - np.log(A)) / (dx)
    return term1 - (term2 * (term3 + term4))

#-----------------------------------------------------------------------------#
#Average partial derivatives for the corrector step; backward difference
def partial_rho_bar(A, rho_0, V_0, dx):
    term1 = rho_0 * ( V_0 - np.roll(V_0, 1)) / (-dx)
    term2 = rho_0 * (V_0 * (np.log(A) - np.log(np.roll(A, 1))) ) / (-dx)
    term3 = V_0 * (rho_0 - np.roll(rho_0, 1)) / (-dx)
    return term1 + term2 + term3

def partial_velocity_bar(rho_0, T_0, V_0, dx, Gamma):
    term1 = V_0 * ( V_0 - np.roll(V_0, 1)) / (-dx)
    term2 = (T_0 - np.roll(T_0, 1)) / (dx)
    term3 = np.asarray(T_0) * (rho_0 - np.roll(rho_0, 1)) / ((dx) * np.asarray(rho_0))
    return term1 - (1/Gamma) * (term2 + term3)

def partial_temp_bar(A, T_0, V_0, dx, Gamma):
    term1 = V_0 * (T_0 - np.roll(T_0,  1)) / (-dx)
    term2 = np.asarray(T_0) * (Gamma - 1)
    term3 = (V_0 - np.roll(V_0, 1)) / (dx) 
    term4 = V_0 * ( np.log(A) - np.log(np.roll(A, 1))) / (dx)
    return term1 - (term2 * (term3 + term4))
#-----------------------------------------------------------------------------#