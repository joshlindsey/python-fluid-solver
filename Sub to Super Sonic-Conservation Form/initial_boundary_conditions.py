# -*- coding: utf-8 -*-
"""
'''
'initial_boundary_conditions' module
    -This module is used to calculate the geometry of the nozzle and implement the
boundary conditions.
'''
"""

#-----------------------------------------------------------------------------#
#Nozzle Area
def Area(x):
    return 1 + 2.2 * (x - 1.5)**2

#-----------------------------------------------------------------------------#
#Initial primivite variables
def Rho_Initial(x):
    if (0 <= x and x <= 0.5):
        return 1
    elif (0.5 <= x and x <= 1.5):
        return 1.0 - 0.366 * (x - 0.5)
    elif (1.5 <= x and x <= 3.0):
        return 0.634 - 0.3879 * (x - 1.5)
        
def Temp_Initial(x):
    if (0 <= x and x <= 0.5):
        return 1
    elif (0.5 <= x and x <= 1.5):
        return 1.0 - 0.167 * (x - 0.5)
    elif (1.5 <= x and x <= 3.0):
        return 0.833 - 0.3507 * (x - 1.5)
    
def Velocity_Initial(x, rho_0, A):
    return 0.59 / (rho_0 * A)

#-----------------------------------------------------------------------------#
#Inflow and outflow boundary conditions
    
def Inflow_U1(area, u1):
    u1[0] = area[0]

def Inflow_U2(u2):
    u2[0] = 2*u2[1] - u2[2]
    
#T_0 is always 1, but make sure that the new velocity is being used in this func
def Inflow_U3(gamma, T_0, V_0, u_1, u_3):
    u_3[0] = u_1[0] * ( (T_0[0] / (gamma - 1)) + ((gamma/2) * (V_0[0])**2 ) )
    
def Outflow_U(u):
    u[-1] = 2*u[-2] - u[-3]
#-----------------------------------------------------------------------------#