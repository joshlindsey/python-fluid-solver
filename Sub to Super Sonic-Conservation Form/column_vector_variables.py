# -*- coding: utf-8 -*-
"""

'''
'column_vector_variables' -module
    -This module is used to represent the variables that simplify primitive 
variables. All of these variables can be simplifed as column matricies of U, F, 
and J to represnet one function to describe the system. Here, U is the solution
vector, F is the flux vector, and J is the source vector. 
    -In addition, barred_U and step_U are used in the predicotr/corrector commpuations.
'''

"""
#-----------------------------------------------------------------------------#
from main import np
#-----------------------------------------------------------------------------#

def Sol_U_1(area, rho_0):
    return rho_0 * area

def Sol_U_2(area, rho_0, V_0):
    return rho_0 * V_0 * area

def Sol_U_3(area, gamma, rho_0, T_0, V_0):
    return rho_0 * area * ( (np.asarray(T_0) / (gamma - 1)) + ((gamma / 2) * np.asarray(V_0)**2) )

#-----------------------------------------------------------------------------#
def Flux_F_1(u_2):
    return u_2

def Flux_F_2(gamma, u_1, u_2, u_3):
    return (u_2**2 / u_1) + ( (gamma - 1)/ gamma) * (u_3 - (gamma/2)*(u_2**2 / u_1))

def Flux_F_3(area, gamma, u_1, u_2, u_3):
    return (gamma * u_2 * u_3 / u_1) - ( ( gamma*(gamma - 1) * (u_2)**3 ) / (2 * (u_1)**2) )

#-----------------------------------------------------------------------------#
def Source_J_2(area, dx, gamma, rho_0, T_0):
    return (1/gamma) * np.asarray(rho_0) * T_0 * (np.roll(area, -1) - area) / dx

#-----------------------------------------------------------------------------#
def Sol_Barred_U(dt, u, pd_u):
    return u + pd_u * dt

def Step_Sol_U(u, pd_u, pd_bar_u, dt):
    return u + 0.5 * (pd_u + pd_bar_u) * dt
#-----------------------------------------------------------------------------#