# -*- coding: utf-8 -*-
"""
'''
'grid_time_deltas'
    -This module is used to initialize the gird and calculate the timestep. C
is the courant number. Feel free to adjust this in the code and watch your program
become unstable. Restrictions were put on delta_t to eleminate the endpoints 
from the calculation of the minimum time step. 
    -List Comprehension is used of x_float, b/c of some sig fig issues.
'''
"""

#-----------------------------------------------------------------------------#
import numpy as np

#-----------------------------------------------------------------------------#
#Computation of time step.
def time_step(T_0, V_0, c, dx):
    delta_t = c * (dx / (np.asarray(T_0)**(1/2) + V_0))[1:-1]
    return min(delta_t)

#-----------------------------------------------------------------------------#
#x step     
def grid(start_x, end_x, dx):
    x_float = np.arange(start_x, end_x + dx, dx)
    return [round(x_float[i], 2) for i in range(len(x_float))] 

#-----------------------------------------------------------------------------#