# -*- coding: utf-8 -*-
"""
'''
'primitive_variables' module;

   -This module is used to calculate the mass flow rate and pressure from the 
primitive variables. In addition, the primitive variables are also decoded here.
"""

#-----------------------------------------------------------------------------#

from main import np

#-----------------------------------------------------------------------------#
#Additional functions calculated from primitive variables.
def Mass_flow(area, rho_0, V_0):
    return area * rho_0 * V_0

def Pressure(rho_0, T_0):
    return rho_0 * np.asarray(T_0)

def Mach_num(V_0, T_0):
    return V_0 / np.sqrt(np.absolute(T_0))

#-----------------------------------------------------------------------------#
#Functions used to decode the primitive variables
def Rho_Decoded(area, u1):
    return u1 / area

def Velocity_Decoded(u_1, u_2):
    return u_2 / u_1

def Temp_Decoded_U(gamma, u_1, u_2, u_3):
    return (gamma - 1) * ((u_3/u_1) - (gamma/2) * (u_2 / u_1)**2 )

def Temp_Decoded_V(gamma, u_1, u_3, V_0):
    return (gamma - 1) * ( (u_3/u_1) - (gamma/2) * (V_0)**2 )
#-----------------------------------------------------------------------------#