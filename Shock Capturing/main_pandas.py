# -*- coding: utf-8 -*-
"""
Created on Sun Mar 3 23:21:57 2019

@author: Josh

    -Caution, be wary of changing the constants. The program fails when C_x 
approaches 0.16. 
    -Gamma is set to 1.4 to match the parameters set by air.
    -The courant number, C is 0.5.

"""

#----------------------------------------------------------------------------------------#
#Import from different modules

import grid_time as delta
import numpy as np
import initial_boundary_conditions as ibc
import column_vector_variables as cvv
import primitive_variables as prim
import partial_derivatives as pd
import timeit
import pandas as panda

#----------------------------------------------------------------------------------------#
if __name__ == "__main__":
    #------------------------------------------------------------------------------------#
    #Constants, Gamma is specifc heat of air, C is Courant number
    #C_x is the coefficeint for artificial viscosity
    Gamma = 1.4
    C = 0.5
    C_x = 0
    C_x_final = 0.3
    D_Cx = 0.1
    
    #Program currently runs 3000 time steps; change as you see fit
    T = 0
    num_time_steps = 1600
    
    #------------------------------------------------------------------------------------#
    #Constants to initialize grid; dx=0.1 -> len(X)=31, dx=0.05 -> len(X)=61
    Dx = 0.05 
    start_x = 0             
    end_x = 3
    
    #------------------------------------------------------------------------------------#
    #Calls funtion to initialize gird and calculate cross-sectionl area
    X = delta.grid(start_x, end_x, Dx)
    Area_array = ibc.Area(np.asarray(X))
   
    #------------------------------------------------------------------------------------#
    #Initial lists for primitive variables; currently, needs to be list b/c x conditions
    Rho_0, Temp_0, Velocity_0 = [], [], []
    R_C, T_C, V_C, P_C, M_C, MF_C = [], [], [], [], [], []
    
    #------------------------------------------------------------------------------------#
    #Iterate density and temperature to establish initial conditions
    #Need to change so I can pass array and not stupid lists; which need long for loops
    for i in range(len(X)):
        Rho_0.append(ibc.Rho_Initial(X[i]))
        Temp_0.append(ibc.Temp_Initial(X[i]))

    #Other primitive variables can now be computed
    Velocity_0 = ibc.Velocity_Initial(X, Rho_0, Area_array)
    Pressure_0 = np.array(prim.Pressure(Rho_0, Temp_0))
    Mass_Flow_Rate_0 = prim.Mass_flow(Area_array, Rho_0, Velocity_0)
    Mach_0 = np.array(prim.Mach_num(Velocity_0, Temp_0))

    #------------------------------------------------------------------------------------#
    #First iteration of primitive variables are stored into memory
    Rho_List = np.array([Rho_0])
    Velocity_List = np.array([Velocity_0])
    Temp_List = np.array([Temp_0])    
    Pressure_List = np.array([Pressure_0])    
    Mass_Flow_Rate_List = np.array([Mass_Flow_Rate_0]) 
    Mach_List = np.array([Mach_0]) 
    
    #------------------------------------------------------------------------------------#
    #Time step is calculated to infer min. time as not to exceed progation of info.
    Dt = delta.time_step(Temp_0, Velocity_0, C, Dx)
    
    #Initial T as number of time steps and begin to timer to clock time of loop
    start_1 = timeit.default_timer()
    while C_x < ( C_x_final + D_Cx) :
        while T < num_time_steps:
            #Primitive variables are recast into the new variables U, F, and J to 
            #simplify the calculations. First the new variables are encoded with the
            #primitive variables, then the next time step is calculated using
            #predictor/corrector technique, finally primitive variables are decoded.
            
            #----------------------------------------------------------------------------#
            #Calculate initial solution variables
            U_1_Old = cvv.Sol_U_1(Area_array, Rho_0)
            U_2_Old = cvv.Sol_U_2(Area_array, Rho_0, Velocity_0)
            U_3_Old = cvv.Sol_U_3(Area_array, Gamma, Rho_0, Temp_0, Velocity_0)
            
            #----------------------------------------------------------------------------#
            #Calculate flux and source terms
            F_1 = cvv.Flux_F_1(U_2_Old)
            F_2 = cvv.Flux_F_2(Gamma, U_1_Old, U_2_Old, U_3_Old)
            F_3 = cvv.Flux_F_3(Area_array, Gamma, U_1_Old, U_2_Old, U_3_Old)
            J_2 = cvv.Source_J_2(Area_array, Dx, Gamma, Rho_0, Temp_0)
    
            #----------------------------------------------------------------------------#
            #Partial derivative are computed for predictor step
            Partial_U_1 = pd.Partial_Flux_U_1(Dx, F_1)
            Partial_U_2 = pd.Partial_Flux_U_2(Dx, F_2, J_2)
            Partial_U_3 = pd.Partial_Flux_U_3(Dx, F_3)
            
            #----------------------------------------------------------------------------#
            #Artificial Viscority
            S_1 = cvv.Artifical_Vis(C_x, Pressure_0, U_1_Old)
            S_2 = cvv.Artifical_Vis(C_x, Pressure_0, U_2_Old)
            S_3 = cvv.Artifical_Vis(C_x, Pressure_0, U_3_Old)
    
            #----------------------------------------------------------------------------#
            #Barred variables or averages for the corrector values
            Bar_U_1 = cvv.Sol_Barred_U(Dt, U_1_Old, Partial_U_1, S_1)
            Bar_U_2 = cvv.Sol_Barred_U(Dt, U_2_Old, Partial_U_2, S_2)
            Bar_U_3 = cvv.Sol_Barred_U(Dt, U_3_Old, Partial_U_3, S_3)
            
            #----------------------------------------------------------------------------#
            #mean values of the primitive variables are required for flux terms
            Rho_Bar = prim.Rho_Decoded(Area_array, Bar_U_1)
            Temp_Bar = prim.Temp_Decoded_U(Gamma, Bar_U_1, Bar_U_2, Bar_U_3)
            
            Bar_F_1 = cvv.Flux_F_1(Bar_U_2)
            Bar_F_2 = cvv.Flux_F_2(Gamma, Bar_U_1, Bar_U_2, Bar_U_3)
            Bar_F_3 = cvv.Flux_F_3(Area_array, Gamma, Bar_U_1, Bar_U_2, Bar_U_3)
            
            #----------------------------------------------------------------------------#
            #Now barred variables are used again for corrector step
            Partial_U_1_Bar = pd.Partial_Flux_U1_Bar(Dx, Bar_F_1)
            Partial_U_2_Bar = pd.Partial_Flux_U2_Bar(Area_array, Dx, Bar_F_2, Gamma, J_2, Rho_0, Temp_0)
            Partial_U_3_Bar = pd.Partial_Flux_U3_Bar(Dx, Bar_F_3)
            
            #----------------------------------------------------------------------------#
            #Calculate pressure bar ans S bar to go into new U's
            Pred_Pressure = cvv.Pressure_Bar(Rho_0, Temp_0, Dx, Dt)
            
            S_1_Bar = cvv.Artifical_Vis(C_x, Pred_Pressure, Bar_U_1)
            S_2_Bar = cvv.Artifical_Vis(C_x, Pred_Pressure, Bar_U_2)
            S_3_Bar = cvv.Artifical_Vis(C_x, Pred_Pressure, Bar_U_3)
            
            #----------------------------------------------------------------------------#
            #Bring it all together to compute next step
            U_1_New = cvv.Step_Sol_U(U_1_Old, Partial_U_1, Partial_U_1_Bar, Dt, S_1_Bar)
            U_2_New = cvv.Step_Sol_U(U_2_Old, Partial_U_2, Partial_U_2_Bar, Dt, S_2_Bar)
            U_3_New = cvv.Step_Sol_U(U_3_Old, Partial_U_3, Partial_U_3_Bar, Dt, S_3_Bar)
            
            #----------------------------------------------------------------------------#
            #Compute the inflow and outflow boundary conditions
            ibc.Inflow_U1(Area_array, U_1_New)
            ibc.Inflow_U2(U_2_New)
            Velocity_i = U_2_New / U_1_New
            ibc.Inflow_U3(Gamma, Temp_0, Velocity_i, U_1_New, U_3_New)
            
            ibc.Outflow_U(U_1_New)
            ibc.Outflow_U(U_2_New)
            ibc.Outflow_U3(Area_array, Gamma, Velocity_i, U_2_New, U_3_New)
            
            #----------------------------------------------------------------------------#
            #Decode for the primitive variables
            Rho_New = prim.Rho_Decoded(Area_array, U_1_New)
            Velocity_New = prim.Velocity_Decoded(U_1_New, U_2_New)
            Temp_New = prim.Temp_Decoded_V(Gamma, U_1_New, U_3_New, Velocity_0)
            
            #----------------------------------------------------------------------------#
            #Calculate the new pressure and mass flow rate
            Pressure_New = np.array(prim.Pressure(Rho_New, Temp_New))
            Mass_Flow_Rate_New = prim.Mass_flow(Area_array, Rho_New, Velocity_New)
            Mach_New = np.array(prim.Mach_num(Velocity_New, Temp_New))
            
            #----------------------------------------------------------------------------#
            #Append the new primitaitve values to list bank
            Rho_List = np.concatenate((Rho_List, [Rho_New]))
            Velocity_List = np.concatenate((Velocity_List, [Velocity_New]))
            Temp_List = np.concatenate((Temp_List, [Temp_New]))
            Pressure_List = np.concatenate((Pressure_List, [Pressure_New]))
            Mass_Flow_Rate_List = np.concatenate((Mass_Flow_Rate_List, [Mass_Flow_Rate_New]))
            Mach_List = np.concatenate((Mach_List, [Mach_New]))
            
            #----------------------------------------------------------------------------#
            #Replace list/primitive variables for next time step
            Rho_0, Temp_0, Velocity_0 = Rho_New, Temp_New, Velocity_New
            Pressure_0, Mass_Flow_Rate_0 = Pressure_New, Mass_Flow_Rate_New
            
            #----------------------------------------------------------------------------#
            #This block of code is to save last elements to compare values for different C_x
            Rho_C , Temp_C, Velocity_C = Rho_List[-1][-1], Temp_List[-1][-1], Velocity_List[-1][-1]
            Pressure_C, Mach_C, Mass_Flow_C = Pressure_List[-1][-1], Mach_List[-1][-1], Mass_Flow_Rate_List[-1][-1]
            
            C_list = [R_C,T_C, V_C, P_C, M_C, MF_C]
            C_list_itt = [Rho_C, Temp_C, Velocity_C, Pressure_C, Mach_C, Mass_Flow_C]
            
            for i in range(len(C_list)):
                C_list[i].append(C_list_itt[i])
                            
            T += 1 
            #----------------------------------------------------------------------------#
            #end of loop 2
            
        
        C_x += D_Cx
        #--------------------------------------------------------------------------------#
        #end of loop 1

#----------------------------------------------------------------------------------------#
    #This block of code is used to display Vairbales at end point to compare different C_x
    data = [{'Density': 0.681, 'Velocity': 0.143, 'Temperature':0.996, 
             'Pressure':0.678,'Mach #':0.143, 'Mass Flow':0.579},
            {'Density': R_C[0], 'Velocity': V_C[0], 'Temperature':T_C[0], 
             'Pressure':P_C[0],'Mach #':M_C[0], 'Mass Flow':MF_C[0] },
            {'Density': R_C[1], 'Velocity': V_C[1], 'Temperature':T_C[1], 
             'Pressure':P_C[1],'Mach #':M_C[1], 'Mass Flow':MF_C[1] },
            {'Density': R_C[2], 'Velocity': V_C[2], 'Temperature':T_C[2], 
             'Pressure':P_C[2],'Mach #':M_C[2], 'Mass Flow':MF_C[2] },
            {'Density': R_C[3], 'Velocity': V_C[3], 'Temperature':T_C[3], 
             'Pressure':P_C[3],'Mach #':M_C[3], 'Mass Flow':MF_C[3] },
             ]
    
    df = panda.DataFrame(data, index=['Exact','C_x=0.0', 'C_x=0.1', 'C_x=0.2', 'C_x=0.3'])
    print(df)
    print()
    
#----------------------------------------------------------------------------------------#
    #stop_1 for time on while loop, start_2 for plotting
    stop_1 = timeit.default_timer()
    print('Time to run while loop: {}'.format(round( stop_1 - start_1 , 4), 's'))
    
#----------------------------------------------------------------------------------------#
#End of script.