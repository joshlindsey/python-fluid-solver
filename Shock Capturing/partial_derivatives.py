# -*- coding: utf-8 -*-
"""
'''
'partial_derivative' module;

   -This module is used to calculate the partial derivatives for both the predictor
and corrector steps. They may seem vary familiar. This is because they are derived 
from the same function; but, the predictor step uses a forward difference and the 
corrector step uses a backward difference.

   -Notice that here these functions are being passed lists and arrays. The functions
use the np.roll() functino to calculate each operation simulatiounsly. There 
maybe some confusion with the np.asarry() function. This is used where the
object being passed is a list instead of an array. Lists can not be operated 
on like arrays; although, if one list is an array, then that array can operate
on a list; hence the np.asarray() function. The np.roll() functions automatically
converst a list to array.
'''

"""

#-----------------------------------------------------------------------------#
from main import np

#-----------------------------------------------------------------------------#
#Partial derivatives for predicotor step
def Partial_Flux_U_1(dx, f_1):
    return - (np.roll(f_1, -1) - f_1) / dx

def Partial_Flux_U_2(dx, f_2, j_2):
    return - (np.roll(f_2, -1) - f_2) / dx + j_2

def Partial_Flux_U_3(dx, f_3):
    return - (np.roll(f_3, -1) - f_3) / dx 

#-----------------------------------------------------------------------------#
#Partial derivatives for corrector step
def Partial_Flux_U1_Bar(dx, f_1):
    return - (f_1 - np.roll(f_1, 1)) / dx

def Partial_Flux_U2_Bar(area, dx, f_2, gamma, j_2 , rho_0, T_0):
    term1 = (f_2 - np.roll(f_2, 1)) 
    term2 = (1/gamma) * np.asarray(rho_0) * T_0 * (area - np.roll(area, 1)) 
    return (-term1 + term2) / dx

def Partial_Flux_U3_Bar(dx, f_3, ):
    return - (f_3 - np.roll(f_3, 1)) / dx 
#-----------------------------------------------------------------------------#