# -*- coding: utf-8 -*-
"""
Created on Sun May  5 15:47:33 2019

@author: Josh
"""
#------------------------------------------------------------------------------------#

import matplotlib.pyplot as plt
import grid_time as delta
from main import np

#------------------------------------------------------------------------------------#
Dx      = 0.01
start_x = 0             
end_x   = 3
X = delta.grid(start_x, end_x, Dx)
#------------------------------------------------------------------------------------#
    
Mass_Flow = np.genfromtxt('Data Files/Mass_Flow_Rate.csv', delimiter=',')


fig, ax = plt.subplots()
ax.plot(X, Mass_Flow, color='black', label='Time Step: {}'.format(7000))
ax.set_xlabel('"Distance through nozzle"')
ax.set_ylabel('"Nondimensional mass flow (p V A/p0 V0 A)"')
plt.title('Variation of Mass Flow through Nozzle')
plt.grid(True)
plt.legend()
ax.set_xlim(0, 3)
ax.set_ylim(0.562, 0.602)
plt.show