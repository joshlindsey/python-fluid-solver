# -*- coding: utf-8 -*-
"""

'''
'column_vector_variables' -module
    -This module is used to represent the variables that simplify primitive 
variables. All of these variables can be simplifed as column matricies of U, F, 
and J to represnet one function to describe the system. Here, U is the solution
vector, F is the flux vector, and J is the source vector. 
    -In addition, barred_U and step_U are used in the predicotr/corrector commpuations.
'''

"""
#-----------------------------------------------------------------------------#
from main import np

#-----------------------------------------------------------------------------#
#Functions for solution vectors

def Sol_U_1(area, rho_0):
    return rho_0 * area

def Sol_U_2(area, rho_0, V_0):
    return rho_0 * V_0 * area

def Sol_U_3(area, gamma, rho_0, T_0, V_0):
    return rho_0 * area * ( (np.asarray(T_0) / (gamma - 1)) + ((gamma / 2) * np.asarray(V_0)**2) )

#-----------------------------------------------------------------------------#
#Functions for flux vectors
    
def Flux_F_1(u_2):
    return u_2

def Flux_F_2(gamma, u_1, u_2, u_3):
    return (u_2**2 / u_1) + ( (gamma - 1)/ gamma) * (u_3 - (gamma/2)*(u_2**2 / u_1))

def Flux_F_3(area, gamma, u_1, u_2, u_3):
    return (gamma * u_2 * u_3 / u_1) - ( ( gamma*(gamma - 1) * (u_2)**3 ) / (2 * (u_1)**2) )

#-----------------------------------------------------------------------------#
#Function for source term
    
def Source_J_2(area, dx, gamma, rho_0, T_0):
    return (1/gamma) * np.asarray(rho_0) * T_0 * (np.roll(area, -1) - area) / dx

#-----------------------------------------------------------------------------#
#Functions for predictor and corrector functions
    
def Sol_Barred_U(dt, u, pd_u, s):
    return u + pd_u * dt + s

def Step_Sol_U(u, pd_u, pd_bar_u, dt, s_bar):
    return u + 0.5 * (pd_u + pd_bar_u) * dt + s_bar
#-----------------------------------------------------------------------------#
#functions required to implement artificial viscosity
    
def Artifical_Vis(c_x, p_0, u):
    term1 = c_x * abs( np.roll(p_0, -1) - 2*p_0 + np.roll(p_0, 1) )
    term2 = np.roll(p_0, -1) + 2*p_0 + np.roll(p_0, 1)
    term3 = np.roll(u, -1) - 2*u + np.roll(u, 1)
    return term1 * term3 / term2

def Pressure_Bar(rho_0, T_0, dx, dt):
    term1 = np.asarray(rho_0) * T_0
    term2 = ( np.roll(T_0, -1) - T_0) * (np.asarray(rho_0) / dx)
    term3 = ( np.roll(rho_0, -1) - rho_0) * (np.asarray(T_0) / dx)
    return term1 + (term2 + term3) * dt   

#-----------------------------------------------------------------------------#
#End of module
    


