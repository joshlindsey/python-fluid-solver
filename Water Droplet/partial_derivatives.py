# -*- coding: utf-8 -*-
"""
Created on Sat Apr 13 19:46:45 2019

@author: Josh
"""
#----------------------------------------------------------------------------------------#
from main import np
#----------------------------------------------------------------------------------------#

def Partial_V_X(dx, grid_y, h, v_y):
    term1 = np.roll(h, -1, axis = 1) - h
    term2 = 0 #grid_y  * v_y
    return - term1 / dx + term2 

def Partial_V_Y(dy, grid_y, h, v_x):
    return - (np.roll(h, -1, axis = 0) - h) / dy - 0 # grid_y  * v_x

def Partial_H_Predictor(dx, dy, v_x, v_y):
    term1 = ( np.roll(v_x, -1, axis = 1) - v_x ) / dx
    term2 = ( np.roll(v_y, -1, axis = 0) - v_y ) / dy
    return - term1 - term2

#----------------------------------------------------------------------------------------#
    
def Partial_vx_bar(dx, grid_y, h_bar, vy_bar):
    return - (h_bar - np.roll(h_bar, 1, axis = 1)) / dx + 0 # grid_y  * vy_bar

def Partial_vy_bar(dy, grid_y, h_bar, vx_bar):
    return - (h_bar - np.roll(h_bar, 1, axis = 0)) / dy - 0 #grid_y  * vx_bar

def Partial_h_bar(dx, dy, vx_bar, vy_bar):
    term1 = (vx_bar - np.roll(vx_bar, 1, axis = 1) ) / dx
    term2 = (vy_bar - np.roll(vy_bar, -1, axis = 0) ) / dy
    return - term1 - term2

#----------------------------------------------------------------------------------------#
    









