"""
Created on Tue Feb 26 20:23:35 2019

Author: Joshua Lindsey

"""
#-----------------------------------------------------------------------------#
import numpy as np
import initial_boundary_conditions as ib_condition
import partial as pd
import grid_time_deltas as delta
import predictor_corrector_press_mach as ewok
import matplotlib.pyplot as plt
import timeit
#To see docstring of any module, uncomment line below
#print( 'import name'.__doc__)
#from matplotlib.animation import FuncAnimation

#-----------------------------------------------------------------------------#
    
if __name__ == "__main__":
    
    #-------------------------------------------------------------------------#
    #Constants that govern type of flow and stability
    C = 0.5                 #Courant Number; largely affects stability
    Gamma = 1.4             #specific heat  for air
    
    #-------------------------------------------------------------------------#
    #Constants to initialize grid
    dx = 0.1               #Step size
    start_x = 0             #starting position
    end_x = 3               #Ending position
    
    #Calls funtion to initialize gird
    X = delta.grid(start_x, end_x, dx)
    
    #-------------------------------------------------------------------------#
    #Data Bank Lists; appended when data needs to be stored
    Area_list, Rho_list, Temp_list, Velocity_list = [], [], [], []
    Pressure_list, Mach_list, Mass_flow_rate = [], [], []
    
    #Lists that the code will be working from and passing data
    Rho_0, Temp_0, Velocity_0 = [], [], []
    Rho_1, Temp_1, Velocity_1 = [], [], []

    
    #-------------------------------------------------------------------------#
    #Functions from modules initial condit... ; appended to list bank
    #First calls functions, appends to 0; second appends 0 value to list bank
    for i in range(len(X)):
        Area_list.append(ib_condition.Area(X[i]))
        
        Rho_0.append(ib_condition.Rho(X[i]))
        Rho_list.append([Rho_0[i]])
        
        Temp_0.append(ib_condition.Temp(X[i]))
        Temp_list.append([Temp_0[i]])
        
        Velocity_0.append(ib_condition.Velocity(X[i], Temp_0[i]))
        Velocity_list.append([Velocity_0[i]])
        
        Pressure_list.append([ewok.pressure(Rho_0[i], Temp_0[i])])
        Mach_list.append([ewok.mach(Velocity_0[i], Temp_0[i])])
        Mass_flow_rate.append([ewok.mass_flow(Rho_0[i], Velocity_0[i], Area_list[i])])
    
    #-------------------------------------------------------------------------#
    #time step is calculated to be used in bar variables
    Dt = delta.time_step(Temp_0, Velocity_0, C, dx)
    
    #-------------------------------------------------------------------------#
    #Initialize t=0 and begin clock to time computer
    T = 0
    start1 = timeit.default_timer()

    while T <= 750:
        #---------------------------------------------------------------------#
        #Partial derivatives are calculated for predictor step
        Partial_Rho = pd.partial_rho(Area_list, Rho_0, Velocity_0, dx)
        Partial_Velocity = pd.partial_velocity(Rho_0, Temp_0, Velocity_0, dx, Gamma)
        Partial_Temp = pd.partial_temp(Area_list, Temp_0, Velocity_0, dx, Gamma)
        
        #---------------------------------------------------------------------#
        #Average variables are calculated to be used in corrector step
        Rho_bar = ewok.rho_bar(Rho_0, Partial_Rho, Dt)
        Velocity_bar = ewok.velocity_bar(Velocity_0, Partial_Velocity, Dt)
        Temp_bar = ewok.temp_bar(Temp_0, Partial_Temp, Dt)
        
        #---------------------------------------------------------------------#
        #Average partial derivatives; corrector step
        Partial_Rho_Bar = pd.partial_rho_bar(Area_list, Rho_bar, Velocity_bar, dx)
        Partial_Velocity_Bar = pd.partial_velocity_bar(Rho_bar, Temp_bar, Velocity_bar, dx, Gamma)
        Partial_Temp_Bar = pd.partial_temp_bar  (Area_list, Temp_bar, Velocity_bar, dx, Gamma)
    
        #---------------------------------------------------------------------#
        #Time marching step is calculated here; calls predictor and corrector step
        Rho_1 = ewok.rho_step(Rho_0, Partial_Rho, Partial_Rho_Bar, Dt)
        Velocity_1 = ewok.velocity_step(Velocity_0, Partial_Velocity, Partial_Velocity_Bar, Dt)
        Temp_1 = ewok.temp_step(Temp_0, Partial_Temp, Partial_Temp_Bar, Dt)
    
        #---------------------------------------------------------------------#
        #Outflow Boundary Conditions
        ib_condition.outflow_boundary(Rho_1)
        ib_condition.outflow_boundary(Velocity_1)
        ib_condition.outflow_boundary(Temp_1)
        
        #Inflow Boundary Conditions
        ib_condition.inflow_boundary_flt(Velocity_1)
        ib_condition.inflow_boundary(Rho_1)
        ib_condition.inflow_boundary(Temp_1)
        
        #---------------------------------------------------------------------#
        #Calculate pressure and mach number from concluded data
        Pressure_1 = ewok.pressure(Rho_1, Temp_1)
        Mach_1 = ewok.mach(Velocity_1, Temp_1)
    
        #---------------------------------------------------------------------#
        #For large calculations, uncomment two lines below to only store incriments of data        
        #if T % 2 == 0 :
            #T_list.append(T)
        for i in range(len(X)):
            Rho_list[i].append(Rho_1[i])
            Temp_list[i].append(Temp_1[i])
            Velocity_list[i].append(Velocity_1[i])
            Pressure_list[i].append(Pressure_1[i])
            Mach_list[i].append(Mach_1[i])
            Mass_flow_rate[i].append(ewok.mass_flow(Rho_1[i], Velocity_1[i], Area_list[i]))
            
           
        #Lists that the code will be working from and passing data
        Rho_0, Temp_0, Velocity_0 = Rho_1, Temp_1, Velocity_1
        Rho_1, Temp_1, Velocity_1 = [], [], []
        
        #print(T)
        T += 1 
    
#-----------------------------------------------------------------------------#
    #From here, all data is stored; below is different plots to visulaize data
    #stop_1 for time on while loop,
    
    print('Time to run while loop: {}'.format(round( timeit.default_timer() - start1 , 4), 's'))
    
    #Set N = 15 to view how variables behave at the throat of the nozzle
    N = -1
    
    fig = plt.figure()
    plt.plot(Rho_list[N], label = 'density')
    plt.xlabel("Number of time steps")
    plt.ylabel("Density (p/p0)")
    plt.title('Density at throat through Time')
    plt.grid(True)
    plt.show()
    
    fig = plt.figure()
    plt.plot(Temp_list[N], label = 'temp')
    plt.xlabel("Number of time steps")
    plt.ylabel("Temperature (T/T0)")
    plt.title('Temperature at throat through Time')
    plt.grid(True)
    plt.show()
    
    fig = plt.figure()
    plt.plot(Velocity_list[N], label = 'velocity')
    plt.xlabel("Number of time steps")
    plt.ylabel("velocity (V/V0)")
    plt.title('Velocity at throat through Time')
    plt.grid(True)
    plt.show()
    
    fig = plt.figure()
    plt.plot(Mach_list[N], label = 'velocity')
    plt.xlabel("Number of time steps")
    plt.ylabel("Mach (M)")
    plt.title('Mach Number at throat through Time')
    plt.grid(True)
    plt.show()
    
    fig = plt.figure()
    plt.plot(Pressure_list[N], label = 'pressure')
    plt.xlabel("Number of time steps")
    plt.ylabel("Pressure (p/p0)")
    plt.title('Pressure at throat through Time')
    plt.grid(True)
    plt.show()
    
    fig = plt.figure()
    plt.plot(X, Area_list, 'b')
    plt.plot(X, (-1)*np.asarray(Area_list), 'b')
    plt.xlabel("Distance (x)")
    plt.ylabel("Height")
    plt.title('Geometry of the Nozzle')
    plt.grid(True)
    plt.show()
    
  
    #-------------------------------------------------------------------------#
    #This block is to plot the mass flow rate at different times
    #Use matrix transpose function to easily convert matrix
    
    #Transpose of matrix to call time steps
    
    #Transpose of matrix to call time steps
    Mass_flow_trans = np.asarray(Mass_flow_rate).transpose()
    
    #Block of code to plot several curves onto one plot
    fig, ax = plt.subplots()
    ax.plot(X, Mass_flow_trans[0], color='green', label='$0$')
    ax.plot(X, Mass_flow_trans[50], color='red', label='$50$')
    ax.plot(X, Mass_flow_trans[100], color='blue', label='$100$')
    ax.plot(X, Mass_flow_trans[150], color='grey', label='$150$')
    ax.plot(X, Mass_flow_trans[200], color='orange', label='$200$')
    ax.plot(X, Mass_flow_trans[700], color='black', label='$700$')
    ax.set_xlabel('"Distance through nozzle"')
    ax.set_ylabel('"Nondimensional mass flow (p V A/p0 V0 A)"')
    plt.title('Instantaneous Distribution of Mass Flow')
    plt.legend()
    plt.grid(True)
    plt.show
        
    #-------------------------------------------------------------------------#
    np.savetxt("data_files/rho_noncon.csv", Rho_list, delimiter=",")
    np.savetxt("data_files/temp_noncon.csv", Temp_list, delimiter=",")
    np.savetxt("data_files/velocity_noncon.csv", Velocity_list, delimiter=",")
    np.savetxt("data_files/pressure_noncon.csv", Pressure_list, delimiter=",")
    np.savetxt("data_files/mass_flow_noncon.csv", Mass_flow_trans, delimiter=",")
    #-------------------------------------------------------------------------#
    #End of code. 
    
    
    