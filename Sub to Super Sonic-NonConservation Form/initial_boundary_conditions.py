# -*- coding: utf-8 -*-
"""
'''

'initial_boundary_conditions' module
    -This module is used to calculate the geometry of the nozzle and implement the
boundary conditions.
'''

"""

#-----------------------------------------------------------------------------#
#nozzle area

def Area(x):
    return 1 + 2.2*(x - 1.5)**2

#-----------------------------------------------------------------------------#
    
#initial conditions
def Rho(x):
    return 1 - 0.3146 * x

def Temp(x):
    return 1 - 0.2314 * x

def Velocity(x, T_0):
    return (0.1 + 1.09 * x) * T_0 **(1/2)

#-----------------------------------------------------------------------------#
    
#boundary conditions
def outflow_boundary(vrt):
    vrt[-1] = 2 * vrt[-2] - vrt[-3]

def inflow_boundary_flt(v):
    v[0] = 2 * v[1] - v[2]
    
def inflow_boundary(rt):
    rt[0] = 1
    
#-----------------------------------------------------------------------------#