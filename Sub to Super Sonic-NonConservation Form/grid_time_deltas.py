# -*- coding: utf-8 -*-
"""

'''
'grid_time_deltas'
    -This module is used to initialize the gird and calculate the timestep.
'''

"""

#-----------------------------------------------------------------------------#
from main import np

#-----------------------------------------------------------------------------#
#timestep

def time_step(T_0, V_0, c, dx):
    return min(c * (dx / (np.asarray(T_0)**(1/2) + V_0)))

#-----------------------------------------------------------------------------#
#x step
    
def grid(start_x, end_x, dx):
    x_float = np.arange(start_x, end_x + dx, dx)
    #List Comprehension on x_float, b/c sig fig issues
    return [round(x_float[i], 1) for i in range(len(x_float))] 

#-----------------------------------------------------------------------------#