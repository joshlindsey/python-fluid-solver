# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 16:27:33 2019

@author: Josh
"""




# This is the code that I used to make the gif animations.
# It works, barely and is here so I do not perminately lose the code.
# To run, copy and paste to the bottom of main and run; this will take a long 
# time and see like you are stuck in a while loop but be paitence.
# This could be MUCH better




'''
fig = plt.figure()

# Query the figure's on-screen size and DPI. Note that when saving the figure to
# a file, we need to provide a DPI for that separately.
print('fig size: {0} DPI, size in inches {1}'.format(
    fig.get_dpi(), fig.get_size_inches()))

plt.xlim(0,31)
plt.ylim(-.5, 2.5)
x = np.arange(0, 31, 1)
y = Mass_flow_trans[0]
line, = plt.plot(x , y, 'b-', linewidth=2)
plt.ylabel("Nondimensional mass flow (p V A/p0 V0 A)")

def update(i):
    label = 'timestep: {}'.format(i)
    print(label)
    # Update the line and the axes (with a new xlabel). Return a tuple of
    # "artists" that have to be redrawn for this frame.
    line.set_ydata(Mass_flow_trans[i])
    plt.xlabel("Distance through nozzle: {}".format(label))
    return line

# FuncAnimation will call the 'update' function for each frame; here
# animating over 10 frames, with an interval of 200ms between frames.

anim = FuncAnimation(fig, update, frames=np.arange(0, 500), interval=100)
anim.save('asd5.gif', dpi=90, writer='imagemagick')
'''