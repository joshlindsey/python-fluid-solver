# -*- coding: utf-8 -*-
"""

'''
'predictor_corrector_press_mach' as ewok module

    -This modlue is a group of functions that do not fall under one category.
The first block is barred variables. This computes the average values that need
to be passed to the corrector step. The second block is where the next time
step is obtained. This is done by adding the predictor and corrector step.
Lastly is three function that needed a home. The pressure, mach, and mass flow
rate are here becasue I didnt know where else to put them.
'''

"""

#-----------------------------------------------------------------------------#
from main import np

#-----------------------------------------------------------------------------#
#barred variables

def rho_bar(rho_0, pd_rho_0, dt):
    return rho_0 + np.asarray(pd_rho_0) * dt

def velocity_bar(V_0, pd_V_0, dt):
    return V_0 + np.asarray(pd_V_0) * dt

def temp_bar(T_0, pd_T_0, dt):
    return T_0 + np.asarray(pd_T_0) * dt

#-----------------------------------------------------------------------------#
#adding the second step
    
def rho_step(rho_0, pd_rho, pd_rho_bar, dt):
    return rho_0 + (0.5) * (pd_rho + pd_rho_bar) * dt

def velocity_step(V_0, pd_V, pd_V_bar, dt):
    return V_0 + (0.5) * (pd_V + pd_V_bar) * dt

def temp_step(T_0, pd_T, pd_T_bar, dt):
    return T_0 + (0.5) * (pd_T + pd_T_bar) * dt 

#-----------------------------------------------------------------------------#
#extra functions for pressure and mach
    
def pressure(rho_0, T_0):
    return np.asarray(rho_0) * T_0

def mach(V_0, T_0):
    return V_0 / np.sqrt(T_0)

def mass_flow(rho_0, V_0, A):
    return np.asarray(rho_0) * V_0 * A

#-----------------------------------------------------------------------------#
#End module