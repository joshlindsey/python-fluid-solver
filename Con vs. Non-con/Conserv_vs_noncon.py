# -*- coding: utf-8 -*-
"""
Created on Sat Apr 20 14:50:42 2019

@author: Josh
"""

#-------------------------------------------------------------------------#

import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d

#-------------------------------------------------------------------------#

def grid(start_x, end_x, dx):
    x_float = np.arange(start_x, end_x + dx, dx)
    #List Comprehension on x_float, b/c sig fig issues
    return [round(x_float[i], 1) for i in range(len(x_float))] 

#-------------------------------------------------------------------------#

Pressure_List_con = np.genfromtxt('data_files/pressure_con.csv', delimiter=',')
Pressure_List_non_con = np.genfromtxt('data_files/pressure_noncon.csv', delimiter=',')
Pressure = np.transpose(Pressure_List_non_con)

Rho_List_con = np.genfromtxt('data_files/rho_con.csv', delimiter=',')
Rho_List_non_con = np.genfromtxt('data_files/rho_noncon.csv', delimiter=',')
Rho = np.transpose(Rho_List_non_con)

Temp_List_con = np.genfromtxt('data_files/temp_con.csv', delimiter=',')
Temp_List_non_con = np.genfromtxt('data_files/temp_noncon.csv', delimiter=',')
Temp = np.transpose(Temp_List_non_con)

Velocity_List_con = np.genfromtxt('data_files/velocity_con.csv', delimiter=',')
Velocity_List_non_con = np.genfromtxt('data_files/velocity_noncon.csv', delimiter=',')
Velocity = np.transpose(Velocity_List_non_con)

Mass_Flow_con = np.genfromtxt('data_files/mass_flow_con.csv', delimiter=',')
Mass_Flow_non_con = np.genfromtxt('data_files/mass_flow_noncon.csv', delimiter=',')

def percent_error(pva):
    act = 0.579
    theo = pva
    return abs((theo - act) / act) * 100
    
#-------------------------------------------------------------------------#

Dx = 0.1
start_x = 0             
end_x = 3

#-------------------------------------------------------------------------#
#Calls funtion to initialize gird and calculate cross-sectionl area
X = grid(start_x, end_x, Dx)
n=15

#-------------------------------------------------------------------------#

fig = plt.figure()
plt.plot(Pressure_List_con[:,[n]], 'dodgerblue', linewidth = 2, label = 'Conservation Form')
plt.plot(Pressure[:,[n]], 'black', linewidth = 2, label = 'Non-Conservation Form')
plt.xlabel("Number of Time Steps")
plt.ylabel("Pressure ")
plt.title('Pressure as a function Time')
plt.grid(True)
plt.legend()
plt.show()

#-------------------------------------------------------------------------#

fig = plt.figure()
plt.plot(Rho_List_con[:,[n]], 'dodgerblue', linewidth = 2, label = 'Conservation Form')
plt.plot(Rho[:,[n]], 'black', linewidth = 2, label = 'Non-Conservation Form')
plt.xlabel("Number of Time Steps")
plt.ylabel("Density")
plt.title('Density as a function Time')
plt.grid(True)
plt.legend()
plt.show()

#-------------------------------------------------------------------------#

fig = plt.figure()
plt.plot(Temp_List_con[:,[n]], 'dodgerblue', linewidth = 2, label = 'Conservation Form')
plt.plot(Temp[:,[n]], 'black', linewidth = 2, label = 'Non-Conservation Form')
plt.xlabel("Number of Time Steps")
plt.ylabel("Temperature")
plt.title('Temperature as a function Time')
plt.grid(True)
plt.legend()
plt.show()

#-------------------------------------------------------------------------#

fig = plt.figure()
plt.plot(Velocity_List_con[:,[n]], 'dodgerblue', linewidth = 2,  label = 'Conservation Form')
plt.plot(Velocity[:,[n]], 'black', linewidth = 2, label = 'Non-Conservation Form')
plt.xlabel("Number of Time Steps")
plt.ylabel("Velocity")
plt.title('Velocity as a function Time')
plt.grid(True)
plt.legend()
plt.show()
#-------------------------------------------------------------------------#

pva_con = percent_error(Mass_Flow_con[-1])
pva_non = percent_error(Mass_Flow_non_con[-1])

non_smoothed = gaussian_filter1d(Mass_Flow_non_con[-1], sigma=2)

pva_non = percent_error(non_smoothed)

fig = plt.figure()
plt.plot(X, pva_con, 'dodgerblue', linewidth = 2,  label = 'Conservation Form')
plt.plot(X, pva_non, 'black', linewidth = 2, label = 'Non-Conservation Form')
plt.xlabel("Distance: x")
plt.ylabel("Percent Error")
plt.title('Mass Flow through the Nozzle')
plt.grid(True)
plt.legend()
plt.show()

fig = plt.figure()
plt.plot(X, Mass_Flow_con[-1], 'dodgerblue', linewidth = 2,  label = 'Conservation Form')
plt.plot(X, non_smoothed, 'black', linewidth = 2, label = 'Non-Conservation Form')
plt.xlabel("x")
plt.ylabel("MassFlow")
plt.title('Mass Flow through the Nozzle')
plt.grid(True)
plt.legend()
plt.show()
#-------------------------------------------------------------------------#











